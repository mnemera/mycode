#!/usr/bin/python3

## for accepting arguments from the cmd line
import argparse

## for making HTTP requests
## python3 -m pip install requests
import requests

## for working with data in lots of formats
## python3 -m pip install pandas
import pandas

ITEMURL = "http://pokeapi.co/api/v2/pokemon/"


def main(): 


    items = requests.get(f"{ITEMURL}?limit=1000")
    items = items.json()

    pokes = [] 

    for i in items['results']: 
        pokes.append(i['name'])
    
    matchedwords = { "matched" :  pokes }
    pokesdf = pandas.DataFrame(matchedwords) 
    
    pokesdf.to_string("pokemons.txt", index=False)
    pokesdf.to_json("pokemons.json")
    pokesdf.to_excel("pokemons.xlsx", index=False)


if __name__ == "__main__":
    main()
