#!/usr/bin/python3
"""Alta3 Research - Exploring OpenAPIs with requests"""
# documentation for this API is at
# https://anapioficeandfire.com/Documentation

import requests
import pprint

AOIF_CHAR = "https://www.anapioficeandfire.com/api/characters/"

#takes a list of urls and gets the name. 
def findName( url ): 
    name_list = [] 

    for each in url:
        x = requests.get(each).json()
        name_list.append(x['name']) 

    return name_list 

def findCharacter():
    done = False
    while not done: 
        got_charToLookup = input("Pick a number between 1 and 1000 to return info on a GoT character!, Type DONE to Quit: " )

        if got_charToLookup.upper() == 'DONE':
            done = True
            break
        if got_charToLookup.upper() == '':
            continue
        gotresp = requests.get(AOIF_CHAR + got_charToLookup).json()
        
        print(f"Character Name: {gotresp['name']}")

        print( 'Allegiance to house: ')
        for allegiance in findName(gotresp['allegiances']): 
            print('  ==> '+allegiance)

        print( 'Appearace in Books: ')
        for book in findName(gotresp['books']):
            print('  ==> '+book)
    

def main(): 
    findCharacter() 

        
def mmain():
        ## Ask user for input
        got_charToLookup = input("Pick a number between 1 and 1000 to return info on a GoT character! " )

        ## Send HTTPS GET to the API of ICE and Fire character resource
        gotresp = requests.get(AOIF_CHAR + got_charToLookup)

        ## Decode the response
        got_dj = gotresp.json()
        print(f"Character Name: {got_dj['name']}")
        print( 'Allegiance to house: ') 
        for allegiance in findName(got_dj['allegiances']):
            #x = requests.get(allegiance).json() 
            print(allegiance) 
        print( 'Appearace in Books: ')
        for book in got_dj['books']:
            x = requests.get(book).json() 
            print(x['name']) 
        #print(f"\tNo. of CHARACTERS -> {len(singlebook['characters'])}\n")
        #pprint.pprint(got_dj)

if __name__ == "__main__":
        main()

